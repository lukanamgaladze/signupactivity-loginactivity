package com.example.signuplogin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity2 : AppCompatActivity() {

    lateinit var email: TextView
    lateinit var pass: TextView
    lateinit var Login: Button
    lateinit var Create: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        email = findViewById(R.id.Email1)
        pass = findViewById(R.id.Password1)
        Login = findViewById(R.id.Login)
        Create = findViewById(R.id.create)

        val Email = intent?.extras?.getString("Email", "")
        val Password = intent?.extras?.getString("Password", "")
        val FirstName = intent?.extras?.getString("FirstName", "")
        val LastName = intent?.extras?.getString("LastName", "")
        val UserName = intent?.extras?.getString("UserName", "")


        Login.setOnClickListener {

            val Email1 = pass.toString()
            val Password1 = email.toString()

            val intent = Intent(this,MainActivity3::class.java)
            intent.putExtra("Email", Email)
            intent.putExtra("Password", Password)
            intent.putExtra("FirstName", FirstName)
            intent.putExtra("LastName", LastName)
            intent.putExtra("UserName", UserName)
            startActivity(intent)

        }

        Create.setOnClickListener{

            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra("Email", Email)
            intent.putExtra("Password", Password)
            intent.putExtra("FirstName", FirstName)
            intent.putExtra("LastName", LastName)
            intent.putExtra("UserName", UserName)
            startActivity(intent)

        }

    }

}