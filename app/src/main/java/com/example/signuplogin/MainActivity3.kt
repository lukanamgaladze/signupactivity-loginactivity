package com.example.signuplogin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class MainActivity3 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main3)

        findViewById<TextView>(R.id.text2).text = intent?.extras?.getString("Email", "")
        findViewById<TextView>(R.id.text3).text = intent?.extras?.getString("Password", "")
        findViewById<TextView>(R.id.text4).text = intent?.extras?.getString("FirstName", "")
        findViewById<TextView>(R.id.text5).text = intent?.extras?.getString("LastName", "")
        findViewById<TextView>(R.id.text6).text = intent?.extras?.getString("UserName", "")

    }
}