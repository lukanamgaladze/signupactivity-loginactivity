package com.example.signuplogin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    lateinit var Email: TextView
    lateinit var Password: TextView
    lateinit var FirstName: TextView
    lateinit var LastName: TextView
    lateinit var UserName: TextView
    lateinit var Register: Button
    lateinit var ExistedAcc: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Email = findViewById(R.id.Email)
        Password = findViewById(R.id.Password)
        FirstName = findViewById(R.id.FirstName)
        LastName = findViewById(R.id.LastName)
        UserName = findViewById(R.id.UserName)
        Register = findViewById(R.id.Register)
        ExistedAcc = findViewById(R.id.ExistedAcc)

        Register.setOnClickListener {
            val Email = Email.text.toString()
            val Password = Password.text.toString()
            val FirstName = FirstName.text.toString()
            val LastName = LastName.text.toString()
            val UserName = UserName.text.toString()

            val intent = Intent(this, MainActivity3::class.java)
            intent.putExtra("Email", Email)
            intent.putExtra("Password", Password)
            intent.putExtra("FirstName", FirstName)
            intent.putExtra("LastName", LastName)
            intent.putExtra("UserName", UserName)
            startActivity(intent)

        }

        ExistedAcc.setOnClickListener {
            val Email = Email.text.toString()
            val Password = Password.text.toString()
            val FirstName = FirstName.text.toString()
            val LastName = LastName.text.toString()
            val UserName = UserName.text.toString()

            val intent = Intent(this, MainActivity2::class.java)
            intent.putExtra("Email", Email)
            intent.putExtra("Password", Password)
            intent.putExtra("FirstName", FirstName)
            intent.putExtra("LastName", LastName)
            intent.putExtra("UserName", UserName)
            startActivity(intent)

        }

    }

}